from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Salsabila Hava Qabita' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 1, 8) #TODO Implement this, format (Year, Month, Date)
npm = 1706979461 # TODO Implement this
major = 'Computer Science'
uni = 'Universitas Indonesia'
hobby = 'surfing the internet and reading'
desc = 'My personality type is INFJ. I am shy, yet open-minded. I really like cars and Formula One. I am also very interested in social issues.'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 
                'major': major, 'uni': uni, 'hobby': hobby, 'desc': desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
